/**Ejercicio02_Unidades_brit�nicas_de_longitud
*   @brief:
*       Sea una determinada cantidad de pulgadas, determinar
*       las millas, furlongs, yardas, pies y pulgadas que representa
*       Consderar:
*           * El valor de las pulgadas a convertir es entre
*             cero y 2 mil millones
*           * Las constantes de conversi�n en pulgadas
*               | Magnitud  | Equivalencia |
*               | --------- | ------------ |
*               | 1 milla   | 63,360in     |
*               | 1 furlong | 7,920in      |
*               | 1 yarda   | 36in         |
*               | 1 pie     | 12in         |
*   @const
*       MILLA   const int   Constante de conversi�n pulgadas-millas
*       FURLONG const int   Constante de conversi�n pulgadas-furlongs
*       YARDA   const int   Constante de conversi�n pulgadas-yardas
*       PIE     const int   Constante de conversi�n pulgadas-pies
*       MIN     const int   Valor m�nimo de la entrada
*       MAX     const int   Valor m�ximo de la entrada
*   @input
*       u   int Cantidad de pulgadas a convertir
*   @output
*       m  int  Cantidad de MILLAS que representa u
*       f  int  Cantidad de FURLONGS que representa u
*       y  int  Cantidad de YARDAS que representa u
*       p  int  Cantidad de PIES que representa u
*       q  int  PULGADAS restantes
*/

#include<stdio.h>
#include<stdlib.h>
#include<locale.h>

/* Rango de la entrada */
#define MIN 0
#define MAX 2000000000
/* Constantes de conversi�n */
const int MILLA = 63360;
const int FURLONG = 7920;
const int YARDA = 36;
const int PIE = 12;

int main()
{
    /* Entradas */
    int u = 0;
    /* Salidas */
    int m = 0;
	int f = 0;
	int y = 0;
	int p = 0;
	int q = 0;

	/* Utilizar caracteres latinos en la salida */
    setlocale(LC_CTYPE, "Spanish");

	/* Entrada */
    printf("Ingresar las pulgadas a convertir: ");
    scanf("%i", &u);
    (u >= MIN && u <= MAX) ? 1 : exit(0);
    printf("%i segundos representa: ", u);
    printf("\n\n");

	/* Proceso  */
	/* Obtener las MILLAS que representa u y las pulgadas restantes */
	m = u/MILLA;
	u = u%MILLA;
	/* Obtener los FURLONGS que representa u y las pulgadas restantes */
	f = u/FURLONG;
	u = u%FURLONG;
	/* Obtener las YARDAS que representa u y las pulgadas restantes */
	y = u/YARDA;
	u = u%YARDA;
	/* Obtener los PIES que representa u y las pulgadas restantes */
	p = u/PIE;
	u = u%PIE;
	/* Obtener las PULGADAS restantes */
	q = u;

	/* Salida */
	printf("%i milla(s), %i furlong(s), %i yarda(s), %i pie(s), %i in(ches)",
           m,f,y,p,q);
    printf("\n");

	return 0;
}
