## Ejercicio 2. Unidades británicas de longitud

### Planteamiento del Problema

Sea una determinada cantidad de **pulgadas**, determinar las **millas**, **furlongs**, **yardas**, **pies**, y **pulgadas** completas que representa. Considerar:

- El valor de las pulgadas a convertir debe estar **entre** 0 y 2 mil millones

| Magnitud | Equivalencia |
| --- | --- |
| 1 milla | 8 furlongs |
| 1 furlong | 220 yardas |
| 1 yarda | 3 pies |
| 1 pie | 12 pulgadas |

### Análisis del Problema

1. Convertir las equivalencias a pulgadas
    1. Convertir de **yardas** a **pulgadas**
        
        **Si**
        
        ```math
            1yarda = 3 pies
        ```
        
        **y**
        
        ```math
            1 pie = 12 pulgas
        ```
        
        **Entonces**
        
        ```math
            1 yarda = 3(12pulgadas)
        ```
        
        **Por lo tanto**
        
        ```math
            1 yarda = 36 pulgadas
        ```
        
    2. Convertir de **furlongs** a **pulgadas**
        
        **Si**
        
        ```math
        1furlong=220yardas
        ```
        
        **y**
        
        ```math
        1 yarda= 36pulgadas
        ```
        
        **Entonces**
        
        ```math
        1furlong=220(36pulgadas)
        ```
        
        **Por lo tanto**
        
        ```math
        1furlong=7,920pulgadas
        ```
        
    3. Convertir de **millas** a **pulgadas**
        
        **Si**
        
        ```math
        1milla=8furlongs
        ```
        
        **y**
        
        ```math
        1furlong=7,920pulgadas
        ```
        
        **Entonces**
        
        ```math
        1milla=8(7,920pulgadas)
        ```
        
        **Por lo tanto**
        
        ```math
        1 milla = 63,360pulgadas
        ```
        
    4. En resumen
        
        
        | Magnitud | Equivalencia |
        | --- | --- |
        | 1 milla | 63,360in |
        | 1 furlong | 7,920in |
        | 1 yarda | 36in |
        | 1 pie | 12in |
2. Determinar la cantidad de **pulgadas** a convertir
    
    ```math
        u = 40,000,000in
    ```
    
3. Obtener las **millas** que representa *u* y las pulgadas restantes
    
    ```math
        m=40,000,000in\div63,360in=631millas
    ```
    
    ```math
        u=40,000,000in\%63,360in=19,840in
    ```
    
4. Obtener los **furlongs** que representa *u* y las pulgadas restantes
    
    ```math
        f = 19,840in\div7,920in=2furlongs
    ```
    
    ```math
        u = 19,840in\%7,920in=4000in
    ```
    
5. Obtener las **yardas** que representa *u* y las pulgadas restantes
    
    ```math
        y = 4000in\div36in=111yardas
    ```
    
    ```math
        u = 4000in\%36in=4in
    ```
    
6. Obtener los **pies** que representa *u* y las pulgadas restantes
    
    ```math
        p=4in\div12in=0
    ```
    
    ```math
        u=4in\%12in=4in
    ```
    
7. Obtener las **pulgadas** restantes
    
    ```math
        q = u = 4in
    ```
    
8. **Requerimientos del sistema**
    
    
    | Entrada | Proceso | Salida |
    | --- | --- | --- |
    | u | m = u / MILLA | 631 millas |
    |  | u = u %MILLA |  |
    |  | f = u /FURLONG | 2 furlongs |
    |  | u = u %FULONG |  |
    |  | y = u /YARDA | 111 yardas |
    |  | u = u %YARDA |  |
    |  | p = u/PIE | 0 pies |
    |  | u = u % PIE |  |
    |  | q = u | 4 pulgadas |
    
    Donde:
    
    | Nombre | Tipo | Descripción |
    | --- | --- | --- |
    | MILLA | const int | Constante de conversión pulgadas-millas |
    | FURLONG | const int | Constante de conversión pulgadas-furlongs |
    | YARDA | const int | Constante de conversión pulgadas-yardas |
    | PIE | const int | Constante de conversión pulgadas-pies |
    | u | int | Cantidad de pulgadas a convertir |
    | m | int | Cantidad de MILLAS que representa u |
    | f | int | Cantidad de FURLONGS que representa u |
    | y | int | Cantidad de YARDAS que representa u |
    | p | int | Cantidad de PIES que representa u |
    | q | int | PULGADAS restantes |
# Referencia
Zaragoza Martínez, F. (2011). 64 EJERCICIOS DE PROGRAMACIÓN (1st ed., p. 4). México: UAM Azcapotzalco.
