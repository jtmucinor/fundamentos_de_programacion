#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

//Factores de conversión
const int YEAR   = 31536000;
const int DAY    = 86400;
const int HOUR   = 3600;
const int MINUTE = 60;

//Valor minumo y máximo para la entrada
#define MIN 0
#define MAX 2000000000

int main()
{
    //ENTRADAS
    int s = 0;
    //SALIDAS
    int a = 0, d = 0, h = 0, m = 0, q = 0;

    //Utilizar caracteres latinos en la salida
    setlocale(LC_CTYPE, "Spanish");

    //Entrada
    printf("Ingresar los segundos a convertir: ");
    scanf("%i", &s);
    (s >= MIN && s <= MAX) ? 1 : exit(0);
    printf("%i segundos representa: ", s);
    printf("\n\n");

    //Proceso
    //Calcular los años que representa s y los segundos restantes
    a = s / YEAR;
    s = s % YEAR;
    //Calcular los días que representa s y los segundos restantes
    d = s / DAY;
    s = s % DAY;
    //Calcular las horas que representa s y los segundos restantes
    h = s / HOUR;
    s = s % HOUR;
    //Calcular los minutos que representa s y los segundos restantes
    m = s / MINUTE;
    s = s % MINUTE;
    //Obtener los segundos restantes
    q = s;

    //Salida
    printf("%i año(s), %i día(s), %i hora(s), %i minuto(s), %i segundo(s)",
           a,d,h,m,q);
    printf("\n");
    return 0;
}
