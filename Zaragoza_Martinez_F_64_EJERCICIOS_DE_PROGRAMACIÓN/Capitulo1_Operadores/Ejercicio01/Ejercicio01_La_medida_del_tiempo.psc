Algoritmo Ejercicio01_La_medida_del_tiempo
	// Factores de conversi�n
	YEAR <- 31536000
	DAY <- 86400
	HOUR <- 3600
	MINUTE <- 60
	// Entrada
	Leer s
	// Proceso 
	// Calcular los a�os que representa s y los segundos restantes
	a <- s/YEAR
	s <- s MOD YEAR
	// Calcular los d�as que representa s y los segundos restantes
	d <- s/DAY
	s <- s MOD DAY
	// Calcular las horas que representa s y los segundos restantes
	h <- s/HOUR
	s <- s MOD HOUR
	// Calcular los minutos que representa s y los segundos restantes
	m <- s/MINUTE
	s <- s MOD MINUTE
	// Obtener los segundos restantes
	q <- s
	// Salida
	Escribir a,' ',d,' ',h,' ',m,' ',q
FinAlgoritmo
