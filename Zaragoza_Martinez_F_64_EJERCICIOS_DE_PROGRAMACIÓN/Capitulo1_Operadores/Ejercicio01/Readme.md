# Ejercicio 01 La medida del tiempo

## Planteamiento del Problema
Sea una determinada cantidad de segundos. Calcular
- Los años, días, horas y minutos completos que representa
- Los segundos restantes al finalizar la conversión anterior

Consideraciones:
- El valor de los segundos debe estar entre 0 y 2 mil millones

|  Magnitud |Equivalencia   |
| :------------ | :------------ |
| 1 minuto  |60 segundos   |
| 1 hora  |60 minutos   |
| 1 día  |24 horas   |
| 1 año  |365 días   |

## Análisis del Problema
1. Convertir los factores de conversión a **segundos**

	a) Convertir de **horas** a **segundos**

		1 hora = 60 minutos
		1 minuto = 60 segundos
		Por lo tanto
		1 hora = 60(60 segundos)
		1 hora = 3,600 segundos

	b) Convertir de **días** a **segundos**

		1 día = 24 horas
		1 hora = 3,600 segundos
		Por lo tanto
		1 día = 24(3,600 segundos)
		1 dia = 86,400 segundos
		
	c) Convertir de **años** a **segundos**
	
		1 año = 365 días
		1 día = 86,400 segundos
		Por lo tanto
		1 año = 365(86,400 segundos)
		1 año = 31, 536, 000 segundos

2. Determinar los **segundos** a **convertir**

	```math
		s = 40,000,000s
	```

3. Calcular los **años** que representa **s** y los segundos restantes

	```math
		a = 40,000,000s\div31,536,000 = 1 anio
	```
	```math
		s = 40,000,000s \% 31,536,000s = 8,464,000s
	```

4. Calcular los **días** que representa **s** y los segundos restantes

	```math
		d = 8,464,000s\div86,400s = 97 dias
		
	```
	```math
		s = 8,464,000s \% 86,400s= 83,200s
	```

5. Calcular las **horas** que representa **s** y los segundos restantes

	```math
		h = 83,200s\div 3,600s = 23 horas
	```
	```math
		s = 83,200s \% 3,600s= 400s
	```
	
6. Calcular los **minutos** que representa **s** y los segundos restantes

	```math
		m =  400s\div 60s =6 minutos
	```
	```math
		s =  400s \% 60s= 40s
	```

7. Obtener los **segundos** restantes

	```math
		q = s = 40s
	```
	

**Tabla de equivalencias**

| Magnitud  | Equivalencia|
| ------------ | --------|
| 1 minuto  | 60 segundos|
| 1 hora  | 3,600 segundos|
| 1 día  | 86,400 segundos|
| 1 año  | 31,536,000 segundos  |

**Requerimientos del sistema**

|  Entrada | Proceso  | Salida  |
| ------------ | ------------ | ------------ |
| s  | a = s / YEAR  | 1 año  |
|   | s = s % YEAR  |   |
|   |  d = s / DAY | 97 días  |
|   | s = s % DAY  |   |
|   |  h = s / HOUR | 23 horas   |
|   | s = s % HOUR  |   |
|   |  m = s / MINUTE | 6 minutos  |
|   | s = s % MINUTE  |   |
|   | q = s  | 40 segundos  |

**Donde:**

|  Nombre | Tipo  | Descripción |
| ------------ | ------------ | ------------ |
| *s*  | **int**  | Cantidad deterinada de segundos a convertir  |
| *a*  | **int**  | Años que representa s   |
| *d*  | **int** | Días que representa s  |
| *h*  | **int** | Horas que representa s  |
| *m*  | **int** | Minutos que representa s   |
| *q*  | **int**  | Segundos restantes después de la conversión  |
| *YEAR*  |  **const int** | Constante de conversión segundos-años  |
| *DAY*  | **const int**  | Constante de conversión segundos-días  |
| *HOUR*  | **const int**  | Constante de conversión segundos-horas  |
| *MINUTE*  | **const int**  | Constante de conversión minutos-horas  |


## Diseño del Algoritmo
### Diagrama de flujo
```	mermaid
flowchart TD;
Start --> id1(YEAR = 31536000);
id1(YEAR = 31536000)-->id2(DAY = 86400);
id2(DAY = 86400)-->id3(DAY = 86400);
id3(DAY = 86400)-->id4(MINUTE = 60);

id4(MINUTE = 60)-->id5[/s/];

id5[/s/]-->id6(a = s/YEAR);
id6(a = s/YEAR)--> id7(s = s/YEAR);
id7(s = s/YEAR)-->id8(d = s/DAY);
id8(d = s/DAY)-->id9(s = s % DAY);
id9(s = s % DAY)-->id10(h = s/HOUR);
id10(h = s/HOUR)-->id11(s = s % HOUR);
id11(s = s % HOUR)-->id12(m = s/MINUTE);
id12(m = s/MINUTE)-->id13(s = s % MINUTE);
id13(s = s % MINUTE)-->id14(q = s)-->id15[/a, d, h, m, q/];
id15[/a, d, h, m, q/]-->Stop

```
### Pseudocódigo
```
Algoritmo Ejercicio01_La_medida_del_tiempo
	// Factores de conversión
	YEAR <- 31536000
	DAY <- 86400
	HOUR <- 3600
	MINUTE <- 60
	// Entrada
	Leer s
	// Proceso 
	// Calcular los años que representa s y los segundos restantes
	a <- s/YEAR
	s <- s MOD YEAR
	// Calcular los días que representa s y los segundos restantes
	d <- s/DAY
	s <- s MOD DAY
	// Calcular las horas que representa s y los segundos restantes
	h <- s/HOUR
	s <- s MOD HOUR
	// Calcular los minutos que representa s y los segundos restantes
	m <- s/MINUTE
	s <- s MOD MINUTE
	// Obtener los segundos restantes
	q <- s
	// Salida
	Escribir a,' ',d,' ',h,' ',m,' ',q
FinAlgoritmo

```
## Codificación del Programa
```c
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

//Factores de conversión
const int YEAR   = 31536000;
const int DAY    = 86400;
const int HOUR   = 3600;
const int MINUTE = 60;

//Valor minumo y máximo para la entrada
#define MIN 0
#define MAX 2000000000

int main()
{
    //ENTRADAS
    int s = 0;
    //SALIDAS
    int a = 0, d = 0, h = 0, m = 0, q = 0;

    //Utilizar caracteres latinos en la salida
    setlocale(LC_CTYPE, "Spanish");

    //Entrada
    printf("Ingresar los segundos a convertir: ");
    scanf("%i", &s);
    (s >= MIN && s <= MAX) ? 1 : exit(0);
    printf("%i segundos representa: ", s);
    printf("\n\n");

    //Proceso
    //Calcular los años que representa s y los segundos restantes
    a = s / YEAR;
    s = s % YEAR;
    //Calcular los días que representa s y los segundos restantes
    d = s / DAY;
    s = s % DAY;
    //Calcular las horas que representa s y los segundos restantes
    h = s / HOUR;
    s = s % HOUR;
    //Calcular los minutos que representa s y los segundos restantes
    m = s / MINUTE;
    s = s % MINUTE;
    //Obtener los segundos restantes
    q = s;

    //Salida
    printf("%i año(s), %i día(s), %i hora(s), %i minuto(s), %i segundo(s)",
           a,d,h,m,q);
    printf("\n");
    return 0;
}
```
## Compilación
## Ejecución del Programa
## Depuración del Programa


# Referencia
Zaragoza Martínez, F. (2011). 64 EJERCICIOS DE PROGRAMACIÓN (1st ed., p. 3). México: UAM Azcapotzalco.
